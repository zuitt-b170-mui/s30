// npm init -y (first step)
// npm install express (second step)
// npm install mongoose (third step)

/* mini activity
    create an express variable that accepts a require ("express") value
    store the "express()" function inside the "app" variable
    create a port variable that accepts 3000
    make your app able to read json as well as accept data from forms

    make your app listen/run to the port variable with a confirmation in the console "server is running at port"
*/

let express = require("express");
const { default: mongoose } = require("mongoose");
const app = express();
const port = 3000;

/* 
    change username to ur username
    change password
    change title to (b170-to-do)
*/
mongoose.connect("mongodb+srv://joshuaniccolo:awesome1@wdc028-course-booking.qwfja.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));

db.once("open", () => console.log("We're connected to the database"))

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Mongoose Schema - sets the structure of the document that is to be created; serves as the blueprint to the data/record
const taskSchema = new mongoose.Schema({
    name: String,
    status:{
        type: String,
        // default - sets the value once the field does not have any value entered in it
        default: "pending"
    }
})

const Task = mongoose.model("Task", taskSchema);

app.post("/tasks", (req,res) => {
    Task.findOne({name:req.body.name}, (error, result) =>{
        if(result !== null && result.name === req.body.name){
            return req.setEncoding("There is a duplicate task")
        } else{
            let newTask = new Task ({
                name: req.body.name
            })
            newTask.save((saveErr, savedTask) =>{
                if (saveErr){
                    return console.error(saveErr);
                } else{
                    return res.status(201).send("New Task Created")
                }
            })
        }
    })
})

app.get("/tasks",(req,res) => {
    Task.find({}, (error,result) => {
        if(error){
            return console.log(error)
        }else{
            return res.status(200).json({data:result})
        }
    })
})

// Activity
const registerSchema = new mongoose.Schema({
    username: String,
    status:{
        type: String,
        default: "pending"
    }
})
const Register = mongoose.model("Register", registerSchema);

app.post("/route", (req,res) => {
    Register.findOne({name:req.body.name}, (error, result) =>{
        if(result !== null && result.name === req.body.name){
            return res.send("Username already in use")
        } else{
            let newRegister = new Register ({
                name: req.body.name
            })
            newRegister.save((saveErr, savedRegister) =>{
                if (saveErr){
                    return console.error(saveErr);
                } else{
                    return res.status(201).send("New User Registered")
                }
            })
        }
    })
})

app.get("/route",(req,res) => {
    Register.find({}, (error,result) => {
        if(error){
            return console.log(error)
        }else{
            return res.status(200).json({data:result})
        }
    })
})

app.listen(port, () => console.log(`Server is running at port ${port}`));